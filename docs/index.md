# Python 조건문: If, Else, Elif <sup>[1](#footnote_1)</sup>

## 목차

1. [소개](./conditional-statements.md#소개)
1. [조건문이란?](./conditional-statements.md#조건문이란)
1. [Python에서 `if` 문 사용법](./conditional-statements.md#python에서-if-문-사용법)
1. [Python에서 `else` 문 사용법](./conditional-statements.md#python에서-else-문-사용법)
1. [Python에서 `elif` 문 사용법](./conditional-statements.md#python에서-elif-문-사용법)
1. [여러 조건을 논리 연산자와 결합하는 방법](./conditional-statements.md#여러-조건을-논리-연산자와-결합하는-방법)
1. [Python에서 중첩(nested) if 문 사용법](./conditional-statements.md#python에서-중첩nested-if-문-사용법)
1. [Python에서 3진 연산자 사용법](./conditional-statements.md#python에서-3진-연산자-사용법)
1. [맺음말](./conditional-statements.md#맺음말)

<a name="footnote_1">1</a>: [Python Tutorial 6 — Python Conditional Statements: If, Else, Elif](https://levelup.gitconnected.com/python-tutorial-6-python-conditional-statements-if-else-elif-ac3a2304a002)를 편역한 것이다.
