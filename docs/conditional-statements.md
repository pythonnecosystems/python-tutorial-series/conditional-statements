# Python 조건문: If, Else, Elif

## 소개
Python 조건문에 대한 이 포스팅에 오신 것을 환영합니다. 여기에서는 Python에서 `if`, `else`와 `elif` 문을 사용하여 서로 다른 조건을 기반으로 프로그램의 흐름을 제어하는 방법을 설명할 것이다. 또한 여러 조건을 논리 연산자와 결합하는 방법과 중첩 if 문과 3진 연산자를 사용하는 방법도 다룰 것이다.

조건문은 분기 논리를 만들고 서로 다른 상황을 기반으로 의사 결정을 내릴 수 있기 때문에 프로그래밍에서 가장 중요한 개념 중 하나이다. 예를 들어 조건문을 사용하여 사용자 입력이 유효한지, 파일이 존재하는지, 숫자가 양수인지 음수인지, 문자열에 특정 단어가 포함되어 있는지 등을 확인할 수 있다.

이 포스팅이 끝나면 Python에서 조건문을 작성하고 다양한 문제와 시나리오에 적용할 수 있다. 

시작하기 전에 Python 구문, 변수, 데이터 타입 및 연산자에 대한 기본적인 이해가 있다고 가정한다. 복습이 필요한 경우 [Python 기본 튜토리얼](https://levelup.gitconnected.com/%5E1%5E)을 참고하세요.

시작할 준비 됐나요? 어서 시작합시다!

## 조건문이란?
조건문은 서로 다른 조건을 바탕으로 프로그램의 흐름을 제어할 수 있게 해주는 문장이다. 즉, 조건문은 어떤 조건이 참인지 거짓인지에 따라 서로 다른 코드 블록을 실행할 수 있도록 해준다.

Python에서 조건문은 `if`, `else`와 `elif` 세 종류가 있다. 이들 문장은 각각 다른 구문과 용례를 가지고 있으며, 이에 대해서는 다음 절에서 살펴볼 것이다.

Python에서 `if` 문의 기본 구문은 다음과 같다.

```python
if condition:
    # do something
```

조건(condition)은 `True` 또는 `False` 중 하나로 평가되는 식이다. 조건이 `True`이면 `if` 문 아래의 코드 블록을 실행한다. 조건이 `False`이면 if 문 아래의 코드 블록의 실핸이 생략된다.

예를 들어 어떤 `number`가 짝수인지 홀수인지를 기준으로 메시지를 출력하고자 한다고 가정해보자. `if` 문을 사용하여 그 `number`가 `2`로 나뉠 수 있는지 확인하고, 만약 그렇다면 `Even`을 출력하고, 그렇지 않으면, `Odd`를 출력한다. 이것을 Python으로 프로그램을 작성하는 방법이다.

```python
# Define a number
number = 10

# Check if the number is even
if number % 2 == 0:
    # Print "Even"
    print("Even")
# Check if the number is odd
else:
    # Print "Odd"
    print("Odd")
```

이 프로그램을 실행하면 `Even`이 출력되는데, `10`은 `2`로 나누어지기 때문이다. 그러나 `number`의 값을 `11`로 바꾸면 `Odd`를 출력하는데, `11`은 `2`로 나누어지지 않기 때문이다.

이것은 Python에서 조건문을 사용하여 분기 논리를 만들고 다양한 상황을 기반으로 의사 결정을 내리는 방법에 대한 간단한 예이다. 다음 절에서 우리는 `else` 문장과 `elif` 문장, 그리고 그것들을 `if` 문장과 결합하여 사용하는 방법에 대해 자세히 설명할 것이다.

## Python에서 `if` 문 사용법
이 절에서는 Python `if` 문을 사용하여 특정 조건이 충족되는 경우에만 코드 블록을 실행하는 방법을 다룰 것이다. 또한 `if` 문을 여러 개 작성하는 방법과 `if` 문의 범위를 정의하기 위해 들여쓰기하는 방법도 설명할 것이다.

`if` 문은 조건이 참인지 거짓인지 확인하고 그에 따라 코드 블록을 실행할 수 있는 조건문이다. Python에서 `if` 문의 기본 구문은 다음과 같다.

```python
if condition:
    # do something
```

조건은 `True` 또는 `False` 중 하나로 평가되는 식이다. 비교, 논리 연산, 멤버쉽 테스트, 함수 호출 등의 유효한 Python 식을 조건으로 사용할 수 있다. 예를 들어, 다음 식을 조건으로 사용할 수 있다.

- `x == y` # x가 y와 같은지
- `x > 10` # x가 10보다 큰지
- `not x` # x가 `False`인지 확인
- `x in y` # x가 y의 원소인지 확인
- `is_prime(x)` # x가 소수인지 확인

조건이 `True`이면 `if` 문 아래의 코드 블록이 실행된다. 조건이 `False`이면 `if` 문 아래의 코드 블록의 실행이 생략된다. 예를 들어 어떤 `number`가 양수인지 음수인지를 기준으로 메시지를 출력하고자 한다고 가정하자. `if` 문을 사용하여 `number`가 `0`보다 큰지 확인하고 `Positive`를 출력한다. 그렇지 않으면 `Not Positive`를 출력한다. Python으로 이 프로그램을 작성하는 방법은 다음과 같다.

```python
# Define a number
number = -5

# Check if the number is positive
if number > 0:
    # Print "Positive"
    print("Positive")
# Check if the number is not positive
else:
    # Print "Not Positive"
    print("Not Positive")
```

`-5`는 `0`보다 크지 않으므로 이 프로그램을 실행하면 `Not Positive`가 출력된다. 그러나 `number`의 값을 `5`로 바꾸면 `Positive`가 나온다. 왜냐하면 `5`는 `0`보다 크기 때문이다.

여러 개의 `if` 문을 연속으로 작성하여 여러 조건을 확인할 수 있다. 예를 들어 어떤 `number`가 `2`, `3` 또는 `5`로 나누어지는지를 기준으로 메시지를 인쇄하고자 한다고 가정해 보겠다. 세 개의 `if` 문을 사용하여 각 조건을 확인하고 참이면 해당 메시지를 출력한다. Python으로 이 프로그램을 작성하는 방법은 다음과 같다.

```python
# Define a number
number = 15

# Check if the number is divisible by 2
if number % 2 == 0:
    # Print "Divisible by 2"
    print("Divisible by 2")
# Check if the number is divisible by 3
if number % 3 == 0:
    # Print "Divisible by 3"
    print("Divisible by 3")
# Check if the number is divisible by 5
if number % 5 == 0:
    # Print "Divisible by 5"
    print("Divisible by 5")
```

위의 프로그램을 실행하면 `15`가 `3`과 `5`로 나누어지기 때문에 `Divisible by 3`과 `Divisible by 5`이 출력된다. 그러나 `number` 값을 `12`로 바꾸면 `Divisible by 2`과 `Divisible by 3`이 출력되는 데, `12`는 `2`와 `3`으로 나누어지기 때문이다.

Python에서 `if` 문에 대해 주의해야 할 한 가지 중요한 점은 들여쓰기한 부분을 사용하여 코드 블록의 범위를 정의한다는 것이다. 들여쓰기한 부분은 코드 한 줄 앞에 있는 공백이나 탭의 양이다. Python에서 들여쓰기한 부분은 함수, 루프 또는 조건문과 같이 어떤 코드 한 줄이 특정 블록에 속하는지 나타내는 데 사용된다. 예를 들어, 다음 코드에서 print문은 4개의 공백으로 들여쓰기한 부분인데, 이는 `if` 문에 속한다는 것을 의미한다.

```python
if x > 0:
    print("Positive")
```

들여쓰기를 제거하면 print 문은 더 이상 `if` 문의 일부가 되지 않으며 `x` 값에 관계없이 항상 실행된다. 예를 들어, 다음 코드에서 print 문은 들여쓰기되지 않았으므로 `if` 문 밖에 있다.

```python
if x > 0:
print("Positive")
```

Python은 `if` 문 뒤에 들여쓰기 블록을 기대하기 때문에 이 코드는 구문 오류를 초래할 것이다. 이 오류를 방지하려면 인쇄 문을 적어도 하나의 공백이나 탭으로 들여쓰기해야 한다.

코드 전체에 일관성 있는 들여쓰기를 사용하는 것이 읽기 쉽고 오류가 발생하지 않도록 하는 좋은 방법이다. Python에서 권장하는 들여쓰기는 레벨당 4칸이지만, 일관성만 있으면 공간이나 탭을 얼마든지 사용할 수 있다. 코드를 자동으로 들여쓰는 IDE나 코드 편집기를 사용할 수도 있다.

이 절에서 우리는 Python에서 `if`문을 사용하여 특정 조건이 충족되는 경우에만 코드 블록을 실행하는 방법을 배웠다. 또한 `if`문을 여러 개 작성하는 방법과 `if`문의 범위를 정의하기 위해 들여쓰기를 사용하는 방법도 배웠다. 다음 절에서 우리는 조건이 충족되지 않으면 Python에서 `else` 문을 사용하여 코드 블록을 실행하는 방법도 배울 것이다.

## Python에서 `else` 문 사용법
이 절에서는 `if` 문의 조건이 충족되지 않는 경우 Python에서 `else` 문을 사용하여 코드 블록을 실행하는 방법을 설명할 것이다. 또한 `if-elif-else` 문의 축약어로 `if-else` 문을 사용하는 방법과 루프가 있는 `else` 문을 사용하는 방법도 배울 것이다.

`else` 문은 `if` 문의 조건이 `False`인 경우 코드 블록을 실행할 수 있는 조건문이다. Python에서 `else` 문의 기본 구문은 다음과 같다.

```python
if condition:
    # do something
else:
    # do something else
```

`else` 문은 항상 `if` 문과 쌍을 이루며, `if` 문 뒤에 나온다. `else` 문은 `if` 문의 조건이 `False`인 경우에만 실행되기 때문에 조건이 없다. 예를 들어 어떤 `number`가 양수인지 음수인지를 기준으로 메시지를 출력하고자 한다고 가정하자. `if` 문장을 사용하여 `number`가 `0`보다 큰지 확인하고 `Positive`를 출력하며, 그렇지 않으면 `else` 문을 사용하여 `Not Positive`를 출력할 수 있다. Python으로 이 프로그램을 작성하는 방법은 다음과 같다.

```python
# Define a number
number = -5

# Check if the number is positive
if number > 0:
    # Print "Positive"
    print("Positive")
# Check if the number is not positive
else:
    # Print "Not Positive"
    print("Not Positive")
```

`-5`는 `0`보다 크지 않으므로 이 프로그램을 실행하면 `Not Positive`이 출력된다. 그러나 `number`의 값을 `5`로 바꾸면 `Positive`가 출력된다. 왜냐하면 `5`는 `0`보다 크기 때문이다.

`if-elif-else` 문을 `if-elif-else` 문의 축약어로 사용하면 여러 조건을 확인하고 그에 따라 다른 코드 블록을 실행할 수 있다. Python에서 `if-elif-else` 문의 기본 구문은 다음과 같다.

```python
if condition1:
    # do something
elif condition2:
    # do something else
elif condition3:
    # do something else
...
else:
    # do something else
```

`elif` 문은 else if의 줄임말로, `if` 문 뒤에 다른 조건이 있는지 확인할 수 있다. 원하는 만큼 `elif` 문을 가질 수 있지만, 마지막에는 하나의 `else` 문만 가질 수 있다. `else` 문은 선택적이며, `True` 조건이 하나도 없는 경우에만 실행된다. 예를 들어, `number`가 양수인지 음수인지 아니면 `0`인지를 기준으로 메시지를 인쇄하고자 한다고 가정해보자. `if` 문을 사용하여 `number`가 `0`보다 큰지 확인하고, `Positive`를 인쇄한다. 그런 다음, `elif` 문을 사용하여 `number`가 `0`보다 작은지 확인하고, `Negative`를 출력한다. 마지막으로, 이전 조건 중 어느 것도 `True`가 아니면 `else` 문을 사용하여 `Zero`를 출력할 수 있다. Python으로 이 프로그램을 작성하는 방법은 다음과 같다.

```python
# Define a number
number = 0

# Check if the number is positive
if number > 0:
    # Print "Positive"
    print("Positive")
# Check if the number is negative
elif number < 0:
    # Print "Negative"
    print("Negative")
# Check if the number is zero
else:
    # Print "Zero"
    print("Zero")
```

이 프로그램을 실행한다면, `0`이` 0`보다 크거나 `0`보다 작지 않기 때문에, `Zero`가 출력될 것이다. 그러나 `number` 값을 5로 바꾸면, `5`가 `0`보다 크기 때문에, `Positive`가 출력된다. 마찬가지로, `number` 값을 `-5`로 바꾸면, `-5`가 `0`보다 작기 때문에, `Negative`가 출려될 것이다.

고리가 있는 다른 문장을 사용하여 고리가 끊기는 문장 없이 정상적으로 끝나는 경우 코드 블록을 실행할 수도 있다. Python에서 고리가 있는 다른 문장의 기본 구문은 다음과 같다.

```python
for item in iterable:
    # do something
else:
    # do something else

while condition:
    # do something
else:
    # do something else
```

`else` 문은 항상 루프와 쌍을 이루며, 루프 다음에 온다. `else` 문은 조건이 없는데, 이는 `break` 문 없이 루프가 정상적으로 종료되는 경우에만 실행되기 때문이다. 예를 들어, 리스트에서 원소를 검색하고, 찾거나 발견하지 못한 경우에 메시지를 인쇄하고자 한다고 가정하자. `for` 루프를 사용하여 리스트를 반복하고, 원소를 발견한 경우 `break` 문을 사용하여 루프를 종료할 수 있다. 그런 다음, `break` 문 없이 루프가 정상적으로 종료되는 경우에 `else` 문을 사용하여 메시지를 출력할 수 있다. Python에서 이 프로그램을 작성하는 방법은 다음과 같다.

```python
# Define a list
my_list = [1, 2, 3, 4, 5]

# Define an element to search for
element = 6

# Iterate over the list
for item in my_list:
    # Check if the item is equal to the element
    if item == element:
        # Print "Found"
        print("Found")
        # Break the loop
        break
# Execute if the loop terminates normally
else:
    # Print "Not Found"
    print("Not Found")
```

프로그램을 실행하면 리스트에 `6`이 없으므로 `Not Found`가 출력된다. 그러나 원소 값을 `4`로 변경하면 리스트에 `4`가 있으므로 `Found`가 출력된다.

이 절에서는 `if` 문의 조건이 충족되지 않는 경우 Python에서 `else` 문을 사용하여 코드 블록을 실행하는 방법을 배웠다. 또한 `if-elif-else` 문의 축약으로 `if-else` 문을 사용하는 방법과 루프가 있는 `else` 문을 사용하는 방법도 배웠다. 다음 절에서는 Python에서 `elif` 문을 사용하여 여러 조건을 확인하고 그에 따라 `else` 코드 블록을 실행하는 방법을 배울 것이다.

## Python에서 `elif` 문 사용법
이 절에서는 Python에서 `elif` 문을 사용하여 여러 조건을 확인하고 그에 따라 `else` 코드 블록을 실행하는 방법을 설명할 것이다. `in` 연산자와 `range` 함수를 사용하고, `elif` 문을 사용하여 멤버십과 범위를 확인하는 방법도 다룰 것이다.

`elif` 문은 `if` 문 뒤에 다른 조건을 확인할 수 있는 조건문이다. Python에서 `elif` 문의 기본 구문은 다음과 같다.

```python
if condition1:
    # do something
elif condition2:
    # do something else
```

`elif` 문은 else if의 줄임말이며, 항상 `if` 문과 쌍을 이룬다. `elif` 문에는 True 또는 False 중 하나로 평가되는 표현식이어야 한다는 조건이 있다. `if` 문의 조건이 `False`이고, `elif` 문의 조건이 `True`이면 `elif` 문 아래의 코드 블록이 실행된다. 두 조건이 모두 `False`이면 elif 문 아래의 코드 블록의 실행은 생략된다. 예를 들어 어떤 숫자가 양수인지, 음수인지, 아니면 제로인지를 기준으로 메시지를 출력하고자 한다고 가정하자. `if` 문을 사용하여 `number`가 `0`보다 큰지 확인하고, `Positive`를 출력한다. 그런 다음, `number`가 `0`보다 작은지 확인하고, `number`가 `0`보다 작으면 `Negative`를 출력한다. Python으로 이 프로그램을 작성하는 방법은 다음과 같다.

```python
# Define a number
number = -5

# Check if the number is positive
if number > 0:
    # Print "Positive"
    print("Positive")
# Check if the number is negative
elif number < 0:
    # Print "Negative"
    print("Negative")
```

이 프로그램을 실행하면 `-5`가 `0`보다 크지 않고 `-5`가 `0`보다 작으므로 `Negative`가 출력된다. 그러나 `number` 값을 `5`로 바꾸면 `Positive`가 출력된다. 왜냐하면 `5`는 `0`보다 크고 `5`는 0보다 작지 않기 때문이다.

여러 조건을 확인하기 위해 원하는 만큼의 `elif` 문을 가질 수 있다. 예를 들어, 글자가 모음인지 자음인지를 기준으로 메시지를 인쇄하고자 한다고 가정해 보자. `if` 문을 사용하여 `letter`가 `"a"`인지 확인하고 그러면 `Vowel`을 출력한다. 그런 다음 5개의 `elif` 문을 사용하여 `letter`가 `"e"`, `"i"`, `"o"`, `"u"`인지 확인하고 `Vowel`을 출력할 수 있다. 마지막으로 이전 조건 중 하나도 참이 아닌 경우 `else` 문을 사용하여 `Consonant`를 출력한다. Python으로 이 프로그램을 작성하면 다음과 같다.

```python
# Define a letter
letter = "b"

# Check if the letter is "a"
if letter == "a":
    # Print "Vowel"
    print("Vowel")
# Check if the letter is "e"
elif letter == "e":
    # Print "Vowel"
    print("Vowel")
# Check if the letter is "i"
elif letter == "i":
    # Print "Vowel"
    print("Vowel")
# Check if the letter is "o"
elif letter == "o":
    # Print "Vowel"
    print("Vowel")
# Check if the letter is "u"
elif letter == "u":
    # Print "Vowel"
    print("Vowel")
# Check if the letter is a consonant
else:
    # Print "Consonant"
    print("Consonant")
```

위의 프로그램을 실행하면, `"b"`가 모음들 중 어떤 것과도 같지 않기 때문에, `Consonant`가 출력될 것이다. 그러나 `letter` 값을 `"a"`로 바꾸면, `"a"`가 첫 번째 조건과 같으므로, `Vowel`이 출력될 것이다. 마찬가지로, 만약 `letter` 값을 `"e"`, `"i"`, `"o"` 또는 `"u"`로 바꾸면 `Vowel`이 출력될 것인데, 왜냐하면 `letter`는 `elif` 조건들 중 하나와 같기 때문이다.

`in` 연산자와 `range` 함수를 사용하여 `elif` 문을 사용하여 구성원과 범위를 확인할 수도 있다. `in` 연산자는 값이 리스트, 튜플, 문자열 또는 범위와 같은 시퀀스의 요소인 경우 `True`를 반환하는 구성원 연산자이다. `range` 함수는 주어진 범위 내의 숫자의 시퀀스를 반환하는 내장 함수이다. 예를 들어, `in` 연산자와 `range` 함수를 사용하여 다음 표현을 사용할 수 있다.

- `x in [1, 2, 3]` # x가 리스트 `[1, 2, 3]`의 요소인지 확인
- `x in "abc"` # x 가 문자열 `"abc"`의 문자인지 확인
- `x in range (10)` # x 가 `0` ~ `9` 범위의 요소인지 확인
- `x in range (5, 10)` # x가 `5` ~ `9` 범위의 요소인지 확인
- `x in range (0, 10, 2)` # x가 `0`에서 `9`까지의 범위에서 `2`씩 증가하는 단계의 요소인지 확인

예를 들어 어떤 숫자가 `1`에서 `10` 사이의 범위에 있는지, `11`에서 `20` 사이의 범위에 있는지, 또는 이 범위 밖에 있는지를 기준으로 메시지를 인쇄하고자 한다고 가정해보자. `if` 문을 사용하여 `number`가 `1`에서 `10` 사이의 범위에 있는지 확인하고, 만약 그렇다면 `In range 1–10`을 출력한다. 그런 다음 `elif` 문을 사용하여 `number`가 `11`에서 `20` 사이의 범위에 있는지 확인하고, 만약 그렇다면 `In range 11–20`을 출력한다. 마지막으로, 이전의 조건 중 어느 것도 `True`가 아니면 `else` 문을 사용하여 `Out of range`를 출력할할 수 있다. Python으로 이 프로그램을 작성하면 다음과 같다.

```python
# Define a number
number = 15

# Check if the number is in the range from 1 to 10
if number in range(1, 11):
    # Print "In range 1-10"
    print("In range 1-10")
# Check if the number is in the range from 11 to 20
elif number in range(11, 21):
    # Print "In range 11-20"
    print("In range 11-20")
# Check if the number is out of range
else:
    # Print "Out of range"
    print("Out of range")
```

위의 프로그램을 실행하면, `15`가 `11`에서 `20` 사이의 범위에 있기 때문에, 출력 `In range 11-20`를 볼 수 있 것이다. 그러나 `number` 값을 `5`로 바꾸면, `5`가 `1`에서 `10` 사이의 범위에 있기 때문에 출력 `In range 1-10`를 볼 수 있 것이다. 마찬가지로, `number` 값을 `25`로 바꾸면, 출력 `Out of range`를 볼 수 있다. 왜냐하면 `25`는 어떤 범위에도 속하지 않기 때문이다.

이 절에서는 Python에서 `elif` 문을 사용하여 여러 조건을 확인하고 그에 따라 다른 코드 블록을 실행하는 방법을 배웠다. 또한 `in` 연산자가 있는 `elif` 문을 사용하여 멤버쉽과 범위를 확인하는 방법도 배웠다. 다음 절에서는 여러 조건을 논리 연산자와 결합하는 방법과 Python에서 중첩 if 문을 사용하는 방법도 배울 것이다.

## 여러 조건을 논리 연산자와 결합하는 방법
이 절에서 Python에서 여러 조건을 논리 연산자와 결합하는 방법을 설명할 것이다. 그리고 연산자가 `and`, `or`와 `not` 를 사용하여 복잡한 조건식을 만드는 방법도 다룰 것이다.

논리 연산자는 두 가지 이상의 조건을 결합하여 하나의 부울 값으로 평가할 수 있는 연산자이다. Python에는 세 가지 논리 연산자가 있다. `and`, `or`, 그리고, `not`이다. 이 연산자들은 각각 다른 의미와 사용 사례를 가지며, 이에 대해서는 다음 절에서 살펴볼 것이다.

### `and` 연산자
`and` 연산자는 두 조건이 모두 True이면 True, 그렇지 않으면 False를 반환하는 논리 연산자이다. Python에서 `and` 연산자의 기본 구문은 다음과 같다.

```python
condition1 and condition2
```

`condition1`과 `condition2`는 True 또는 False 중 하나로 평가하는 표현식이다. 비교, 논리 연산, 멤버십 테스트, 함수 호출 등과 같은 유효한 Python 표현식을 조건으로 사용할 수 있다. 예를 들어, `and` 연산자와 함께 다음과 같은 표현식을 사용할 수 있다.

- `x > 0 and x < 10` # x가 0과 10사이인지
- `x == y and x != z` # x 가 y 와 같고 z 와 같지 않은지
- `not x and not y` # x와 y가 모두 False인지
- `x in y and y in z` # 에서 x 가 y 의 원소이고 y 가 z 의 원소인지
- `is_prime(x) and is_even(x)` # x가 소수이고 짝수인지

두 조건이 모두 True이면 `and` 연산자는 True를 반환한다. 두 조건 중 하나 또는 둘 다 False이면 `and` 연산자는 False를 반환한다. 예를 들어 숫자가 1과 10 사이에 있고 2로 나눌 수 있는지 여부를 기준으로 메시지를 인쇄하고 싶다고 가정해 보겠다. if 문을 사용하여 두 조건이 모두 충족되는지 확인하고 두 조건이 충족되면 "Yes"를 인쇄한다. 그렇지 않으면 "No"를 인쇄한다. 이 프로그램을 Python으로 작성하면 다음과 같습니다.

```python
# Define a number
number = 6

# Check if the number is between 1 and 10, and divisible by 2
if number > 0 and number < 11 and number % 2 == 0:
    # Print "Yes"
    print("Yes")
# Check if the conditions are not met
else:
    # Print "No"
    print("No")
```

위 프로그램을 실행하면 `Yes`가 출력되는데, `6`은 1과 10 사이이고 `2`로 나눌 수 있기 때문이다. 그러나 `number` 값을 `7`로 바꾸면 `No`가 출력되는데, `7`은 `2`로 나눌 수 없기 때문이다. 마찬가지로 `number` 값을 `12`로 바꾸면 `No`가 출력되는데, `12`는 1과 10사이가 아니기 때문이다.

원하는 만큼 연산자를 사용하여 여러 조건을 결합할 수 있다. 그러나 ₩뭉`and` 연산자는 왼쪽에서 오른쪽으로 평가되고 False 조건을 발견하는 즉시 중지되므로 평가 순서에 주의해야 한다. 이를 단락 평가(short-circuit evaluation)라고 하며 코드의 성능과 효율을 향상시킬 수 있다. 예를 들어 다음 식에서는 첫 번째 조건이 False이기 때문에 두 번째 조건이 평가되지 않다:

```python
False and some_function() # the function is not called
```

그러나 다음 식에서는 첫 번째 조건이 True이므로 두 번째 조건을 평가한다.

```python
True and some_function() # the function is called
```

따라서 `and` 연산자를 사용할 때 가장 가능성이 높거나 간단한 조건을 먼저, 가장 가능성이 낮거나 복잡한 조건을 마지막에 두는 것이 좋은 방법이다. 이렇게 하면 불필요한 평가를 피할 수 있고 시간과 자원을 절약할 수 있다.

### `or` 연산자
`or` 연산자는 논리 연산자로, 둘 중 하나 또는 둘 다 조건이 True이면 True, 그렇지 않으면 False를 반환한다. Python에서 `or` 연산자의 기본 구문은 다음과 같다.

```python
condition1 or condition2
```

`condition1`과 `condition2`는 True 또는 False 중 하나를 평가하는 표현식이다. 비교, 논리 연산, 멤버십 테스트, 함수 호출 등의 유효한 Python 표현식을 조건으로 사용할 수 있다. 예를 들어, `or` 연산자와 함께 다음 표현식을 사용할 수 있다.

- `x > 0 or x < 0` # x가 0이 아닌지
- `x == y or x == z` # x가 y 또는 z 중 하나인지
- `not x or not y` # x 또는 y 중 하나가 False인지
- `x in y or z in y` # x 또는 z 중 하나가 y의 원소인지
- `is_prime(x) or is_odd(x)` # x가 소수인지 홀수인지

두 조건 중 하나 또는 둘 다 True이면 또는 연산자는 True를 반환한다. 두 조건 모두 False이면 `or` 연산자는 False를 반환한다. 예를 들어 어떤 숫자가 2나 3으로 나뉠 수 있는지를 기준으로 메시지를 인쇄하고자 한다고 가정해 보겠다. if 문을 사용하여 두 조건 중 하나가 충족되는지 확인하고, 충족되면 "Yes"를 출력한다. 그렇지 않으면 "No"를 출력한다. 이 프로그램을 Python으로 작성하면 다음과 같다.

```python
# Define a number
number = 9

# Check if the number is divisible by 2 or 3
if number % 2 == 0 or number % 3 == 0:
    # Print "Yes"
    print("Yes")
# Check if the conditions are not met
else:
    # Print "No"
    print("No")
```

위 프로그램을 실행하면 `Yes`가 출력되는데, `9`는 `3`으로 나눌 수 있기 때문이다. 그러나 `number` 값을 `10`으로 바꾸면 `Yes`가 출력되는데, `10`은 `2`로 나누어지기 때문이다. 마찬가지로 `number` 값을 `11`로 바꾸면 `No`가 출력되는데, `11`은 `2`나 `3`으로 나눌 수 없기 때문이다.

원하는 만큼 `or` 연산자를 사용하여 여러 조건을 결합할 수 있다. 그러나 `or` 연산자는 왼쪽에서 오른쪽으로 평가되고 True 조건을 찾는 즉시 중지되므로 평가 순서에 주의해야 한다. 이를 단락 평가라고도 하며 코드의 성능과 효율을 향상시킬 수 있다. 예를 들어 다음 식에서는 첫 번째 조건이 True이므로 두 번째 조건을 평가하지 않는다.

```python
True or some_function() # the function is not called
```

그러나 다음 식에서는 첫 번째 조건이 False이기 때문에 두 번째 조건을 평가한다.

```python
False or some_function() # the function is called
```

따라서 `or` 연산자를 사용할 때 가장 가능성이 높거나 간단한 조건을 먼저, 가장 가능성이 낮거나 복잡한 조건을 마지막에 두는 것이 좋은 관행이다. 이렇게 하면 불필요한 평가를 피할 수 있고 시간과 자원을 절약할 수 있다.

### `mot` 연산자
`not` 연산자는 조건의 반대를 반환하는 논리 연산자이다. Python에서 `not` 연산자의 기본 구문은 다음과 같다.

```python
not condition
```

조건은 True 또는 False 중 하나로 평가되는 식이다. 비교, 논리 연산, 멤버쉽 테스트, 함수 호출 등의 유효한 Python 표현식을 조건으로 사용할 수 있다. 예를 들어 `not` 연산자와 함께 다음 식을 사용할 수 있다.

- `not x > 0` # x가 0보다 크지 않은지
- `not x == y` # x가 y와 같지 않은지
- `not x` # x가 거짓인지
- `not x in y` # x가 y의 원소가 아닌지
- `not is_prime(x)` # x가 소수가 아닌지

조건이 True이면 `not` 연산자는 False를 반환한다. 조건이 False이면 `not` 연산자는 True를 반환한다. 예를 들어 어떤 숫자가 2로 나뉠 수 없는지를 기준으로 메시지를 출력하고자 한다고 가정하자. if 문을 사용하여 조건이 충족되는지 확인하고, 충족되면 `Yes`를 출력한다. 그렇지 않으면 `No`를 출력한다. 이 프로그램을 Python으로 작성하면 다음과 같다.

```python
# Define a number
number = 15

# Check if the number is not divisible by 2
if not number % 2 == 0:
    # Print "Yes"
    print("Yes")
# Check if the condition is not met
else:
    # Print "No"
    print("No")
```

이 예에서 프로그램은 `not` 연산자를 사용하여 숫자가 2로 나누어지지 않는지 확인한다. 만약 True이면 `Yes`, 그렇지 않으면 `No`, 를 출력한다. 프로그램을 실행하면, `15`는 `2`로 나누어지지 없으므로 `Yes`라는 출력이 나올 것이다.

`not` 연산자는 조건을 부정하고자 할 때 특히 유용하며, 코드를 더 잘 표현하고 읽을 수 있게 해준다. 비교, 논리 연산, 멤버십 테스트 또는 함수 호출을 포함한 모든 조건에 적용할 수 있어 주어진 조건의 반대를 표현하는 데 유연성을 제공한다.

`and` 또는 `or`와 같이 `not` 논리 연산자를 결합하면 프로그램에서 복잡한 조건을 만들고 더 정교한 제어 흐름을 만들 수 있다는 점에 주목할 필요가 있다. 이러한 논리 연산자를 이해하고 사용하면 복잡한 논리를 간결하고 명확하게 표현할 수 있다.

## Python에서 중첩(nested) if 문 사용법
이 절에서는 Python에서 중첩 if 문을 사용하여 더 복잡한 조건부 논리를 만드는 방법을 다룰 것이다. 또한 코드를 더 읽기 쉽고 오류를 방지하기 위해 들여쓰기와 괄호를 사용하는 방법도 설명할 것이다.

중첩된 if 문은 if 문 안에 다른 if 문이 있는 문이다. Python에서 중첩 if 문의 기본 구문은 다음과 같다.

```python
if condition1:
    # do something
    if condition2:
        # do something else
```

`condition1`과 `condition2`는 True 또는 False 중 하나를 평가하는 식이다. 비교, 논리 연산, 멤버쉽 테스트, 함수 호출 등의 유효한 Python 표현식을 조건으로 사용할 수 있다. 예를 들어, 다음 표현식을 조건으로 사용할 수 있다.

- `x > 0 and x < 10` # x가 0과 10사이인지
- `x == y and x != z` # x 가 y 와 같고 z 와 같지 않은지
- `not x and not y` # x와 y가 모두 False인지
- `x in y and y in z` # x 가 y 의 원소이고 y 가 z 의 원소인지
- `is_prime(x) and is_even(x)` # x가 소수이고 짝수인지

`condition1`이 True이면 첫 번째 if 문 아래의 코드 블록이 실행된다. 그 다음 `condition2`도 True이면 두 번째 if 문 아래의 코드 블록을 실행한다. 두 조건 중 하나 또는 둘 다 False이면 두 번째 if 문 아래의 코드 블록의 실행은 생략된다. 예를 들어 숫자가 1과 10 사이에 있고 2로 나누어지는지 여부를 기반으로 메시지를 출력한다고 가정하자. 중첩 if 문을 사용하여 두 조건이 모두 충족되는지 확인하고 해당 조건이 충족되면 "Yes"를 출력한다. 그렇지 않으면 "No"를 출력한다. 이 프로그램을 Python으로 작성하면 다음과 같다.

```python
# Define a number
number = 6

# Check if the number is between 1 and 10
if number > 0 and number < 11:
    # Check if the number is divisible by 2
    if number % 2 == 0:
        # Print "Yes"
        print("Yes")
    # Check if the number is not divisible by 2
    else:
        # Print "No"
        print("No")
# Check if the number is not between 1 and 10
else:
    # Print "No"
    print("No")
```

위의 프로그램을 실행하면 `Yes`가 출력되는데, `6`은 `1`과 `10`사이이고 `2`로 나눌 수 있기 때문이다. 그러나 `number` 값을 `7`로 바꾸면 `No`가 출력되는데, `7`은 `2`로 나눌 수 없기 때문이다. 마찬가지로 `number` 값을 `12`로 바꾸면 `No`가 출력되는데, `12`는 `1`과 `10`사이가 아니기 때문이다.

더 복잡한 조건부 논리를 만들기 위해 중첩 if 문을 원하는 만큼 가질 수 있다. 그러나 들여쓰기는 중첩 if 문의 범위를 정의하기 때문에 주의해야 한다. 들여쓰기는 코드 한 줄 앞에 있는 공백이나 탭의 양을 나타낸다. Python에서 들여쓰기는 함수, 루프 또는 조건부 문과 같은 특정 블록에 속하는 코드의 어떤 줄을 나타내는데 사용된다. 예를 들어 다음 코드에서 두 번째 if 문은 4개의 공백으로 들여쓰기되었으며, 이는 첫 번째 if 문에 속한다는 것을 의미한다.

```python
if x > 0:
    if x < 10:
        print("Positive and less than 10")
```

들여쓰기를 제거하면 두 번째 if 문은 더 이상 첫 번째 if 문에 속하지 않고 독립적으로 실행된다. 예를 들어, 다음 코드에서 두 번째 if 문을 들여쓰기하지 않으면, 이는 첫 번째 if 문 밖에 있음을 의미한다.

```python
if x > 0:
if x < 10:
    print("Positive and less than 10")
```

Python은 첫 번째 if 문 뒤에 들여쓰기 블록을 기대하기 때문에 이 코드는 구문 오류를 초래할 것이다. 이 오류를 방지하려면 두 번째 if 문을 적어도 하나의 공백이나 탭으로 들여쓰기해야 한다.

코드 전체에 일관성 있는 들여쓰기를 사용하는 것이 읽기 쉽고 오류가 발생하지 않도록 하는 좋은 방법이다. Python에서 권장하는 들여쓰기는 레벨당 4칸이지만, 일관성만 있으면 공간이나 탭을 얼마든지 사용할 수 있다. 코드를 자동으로 들여쓰는 IDE나 코드 편집기를 사용할 수도 있다.

코드를 더 읽기 쉽게 만들고 오류를 방지하기 위해 괄호를 사용할 수도 있다. 괄호는 표현식들을 그룹화하여 평가의 순서를 나타내는 기호이다. Python에서 괄호는 연산자의 우선 순위, 왼쪽에서 오른쪽으로 평가와 같이 연산의 기본 순서를 재정의하는 데 사용된다. 예를 들어 다음 식에서 괄호는 곱셈 전에 덧셈이 수행됨을 나타낸다.

```python
x = (2 + 3) * 4 # x is 20
```

괄호가 없으면 식이 다르게 평가될 것이고, 결과도 다를 것이다. 예를 들어, 다음 식에서 곱셈 연산자는 덧셈 연산자보다 우선순위가 높기 때문에 덧셈 전에 곱셈을 수행한다.

```python
x = 2 + 3 * 4 # x is 14
```

괄호를 사용하여 조건을 함께 그룹화하고 평가의 순서를 나타낼 수 있다. 예를 들어, 다음 식에서 괄호는 `and` 연산자가 `or` 연산자보다 먼저 평가됨을 나타낸다.

```python
x = (a and b) or c # x is True if either a and b are both True, or c is True
```

괄호가 없으면 식이 다르게 평가될 것이고, 결과도 다를 것이다. 예를 들어, 다음 식에서, `or` 연산자는 `and` 연산자보다 우선순위가 낮기 때문에, `and` 연산자가 `or` 연산자 먼저 평가된다.

```python
x = a and b or c # x is True if either a and b are both True, or c is True
```

괄호를 사용하여 코드를 더 읽기 쉽게 만들고 오류를 방지할 수도 있다. 예를 들어, 다음 코드에서 괄호는 두 번째 if 문이 첫 번째 if 문 안에 중첩되어 있음을 분명히 한다.

```python
if (x > 0):
    if (x < 10):
        print("Positive and less than 10")
```

괄호가 없으면 코드는 여전히 작동하지만 읽고 이해하기가 더 어려울 수 있다. 예를 들어, 다음 코드에서 들여쓰기는 두 번째 if 문이 첫 번째 if 문 안에 중첩되어 있다는 유일한 표시이다.

```python
if x > 0:
    if x < 10:
        print("Positive and less than 10")
```

따라서 괄호를 사용하여 조건을 함께 그룹화하고 평가 순서를 표시하여 코드를 더 읽기 쉽게하고 오류를 방지하는 것이 좋다.

이 절에서 우리는 Python에서 중첩 if 문을 사용하여 더 복잡한 조건부 논리를 만드는 방법을 배웠다. 또한 코드를 더 읽기 쉽게 만들고 오류를 방지하기 위해 들여쓰기와 괄호를 사용하는 방법도 배웠다. 다음 절에서 우리는 Python에서 3진 연산자를 사용하여 간결하고 우아한 조건부 표현을 쓰는 방법도 배울 것이다.

## Python에서 3진 연산자 사용법
이 절에서는 Python에서 3진 연산자를 사용하여 간결하고 우아한 조건식을 작성하는 방법을 설명할 것이다. 람다(lambda) 함수와 리스트 컴프리헨션(list compreshension)에서 3진 연산자를 사용하는 방법도 다룰 것이다.

삼진 연산자는 세 개의 피연산자를 취하고 조건에 따라 단일 값을 반환하는 연산자이다. Python에서 삼진 연산자의 기본 구문은 다음과 같다.

```python
value_if_true if condition else value_if_false
```

`condition`은 True 또는 False 중 하나로 평가되는 식이다. `value_if_true`와 `value_if_false`는 `condition`을 기반으로 값을 반환하는 식이다. 비교, 논리 연산, 멤버쉽 테스트, 함수 호출, 리터럴 값 등과 같은 유효한 Python 표현식을 조건 또는 값으로 사용할 수 있다. 예를 들어, 3진 연산자와 함께 다음 식을 사용할 수 있다.

- `if x > 0 else -x` # x의 절대값을 반환한다
- `"Yes if x == y else "No"` # x가 y와 같으면 "Yes"를 반환하고, 그렇지 않으면 "No"를 반환한다
- `max(x, y) if x > y else min(x, y)` # x가 y보다 크면 x와 y의 최대값을 반환하고, 그렇지 않으면 x와 y의 최소값을 반환한다
- `x[0] if x else None` # x가 비어있지 않으면 x의 첫 번째 요소를 반환하고, 그렇지 않으면 `None`을 반환한다
- `random.choice(x) if x else 0` # x가 비어 있지 않으면 x의 임의 요소를 반환하고, 그렇지 않으면 0을 반환한다

`condition`이 True이면 삼진 연산자는 `value_if_true`를 반환한다. `condition`이 False이면 삼진 연산자는 `value_if_false`를 반환한다. 예를 들어 어떤 학생의 점수를 바탕으로 성적을 부여하고자 한다고 가정하자. 점수가 90 이상이면 삼진 연산자를 사용하여 "A"를 부여하고, 그렇지 않으면 "B"를 부여한다. Python으로 이 프로그램을 작성하면 다음과 같다.

```python
# Define a score
score = 95

# Assign a grade based on the score
grade = "A" if score >= 90 else "B"

# Print the grade
print(grade)
```

이 프로그램을 실행하면 `95`가 `90`보다 크거나 같으므로 `A`를 출력한다. 그러나 `score` 값을 `85`로 바꾸면, `85`는 `90`보다 크거나 같지 않기 때문에 `B`를 출력한다.

`if-else` 문 대신에 3진 연산자를 사용하여 간결하고 우아한 조건식을 작성할 수 있다. 3진 연산자는 조건에 따라 단일 값으로 평가하기 때문에 조건식이라고도 불린다. 3진 연산자는 조건에 따라 변수에 값을 할당하거나 함수에서 값을 반환하거나 함수에 인수로 값을 전달하려는 경우에 유용할 수 있다. 예를 들어 숫자의 부호를 반환하는 함수를 작성하고자 한다고 가정하자. 3진 연산자를 사용하여 숫자가 양수이면 1, 음수이면 -1, 0이면 0을 반환할 수 있다. 이 함수를 Python으로 작성하면 다음과 같다.

```python
# Define a function that returns the sign of a number
def sign(number):
    # Return 1 if the number is positive, -1 if the number is negative, or 0 if the number is zero
    return 1 if number > 0 else -1 if number < 0 else 0

# Test the function
print(sign(5)) # 1
print(sign(-3)) # -1
print(sign(0)) # 0
```

람다 함수와 리스트 컴프리헨션이 있는 삼진 연산자를 사용하여 조건에 따라 익명의 함수와 리스트를 만들 수도 있다. 람다 함수는 이름 없이 정의되는 함수로, 임의의 수의 인수를 사용하여 단일 값을 반환할 수 있다. 리스트 컴프리헨션은 반복 가능한 리스트, 튜플, 문자열 또는 범위에서 각 요소에 표현식을 적용하여 리스트를 만드는 방법이다. 예를 들어 삼진 연산자, 람다 함수 및 리스트 컴프리헨션가 있는 경우 다음 식을 사용할 수 있다.

- `lambda x: x if x>0 else -x` # x의 절대값을 반환하는 람다 함수
- `[x if x > 0 else - x for x in my_list]` # my_list 요소의 절대값 리스트를 반환하는 리스트 컴프리헨션
- `lambda x: "Yes" if x == y else "No"` # x가 y와 같으면 "Yes"를 반환하고, 그렇지 않으면 "No"를 반환하는 람다 함수
- `["Yes" if x == y else "No" for x in my_list]` # my_list의 요소가 y와 동일한지 여부를 기준으로 "Yes" 또는 "No"의 리스트를 반환하는 리스트 컴프리헨션
- `lambda x: max(x, y) if x > y else min(x, y)` # x가 y보다 크면 x와 y의 최대값을 반환하고 그렇지 않으면 x와 y의 최소값을 반환하는 람다 함수
- `[max(x, y) if x > y else min(x, y) for x in my_list]` # my_list 및 y의 요소 중 최대 또는 최소의 리스트를 반환하는 리스트 컴프리헨션

람다 함수와 리스트 컴프리헨션에 대해 자세히 알고 싶다면 [Python Lambda Functions Tutorial](https://levelup.gitconnected.com/%5E2%5E)과 [Python List Comprehension Tutorial](https://levelup.gitconnected.com/%5E3%5E)을 읽어 보세요.

이 절에서는 Python 3진 연산자를 사용하여 간결하고 우아한 조건식을 작성하는 방법을 배웠다. 람다 함수 및 리스트 컴프리헨션과 함께 3진 연산자를 사용하는 방법도 배웠다.

## 맺음말
축하합니다! Python 조건문에 대한 이 포스팅을 완료했다. 이 글에서는 Python에서 if, elif 문을 사용하여 서로 다른 조건을 기반으로 프로그램의 흐름을 제어하는 방법을 배웠다. 또한 여러 조건을 논리 연산자와 결합하는 방법과 Python에서 중첩 if 문과 삼진 연산자를 사용하는 방법도 배웠다.

Python에서 조건문을 사용하는 방법을 익힘으로써 프로그래밍과 데이터 분석에서 다양한 문제와 시나리오를 해결하는 데 도움이 되는 소중한 기술을 얻게 되었다. 조건문은 분기 논리를 만들고 다양한 상황을 기반으로 의사 결정을 내릴 수 있기 때문에 프로그래밍에서 가장 중요한 개념 중 하나이다.

이 글이 유익했기를 바랍니다. Python 프로그래밍과 데이터 분석에 대해 더 알고 싶다면 웹사이트에서 다른 튜토리얼을 참고할 수 있다. 또한 뉴스레터를 구독하고 소셜 미디어를 팔로우하면 최신 업데이트와 소식을 얻을 수 있다.
